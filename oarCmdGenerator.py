#!/usr/bin/env python
# coding: utf-8

import sys 
import inspect 
import begin 
import os

"""
 Créé le mercredi  4 mai 2016.

 raccourci oarsubv

"""

@begin.start
def main(gpu=False, time=20, core=1, command='""', theano=False, besteffort=False, host=None, cuda=False, notify=False, mail=None, run=False, python=False, interactive=False, anterior=None, flags=''):
    p=' -p "'
    if gpu: 
        p+='gpu IS NOT NULL '
    else:
        p+='gpu IS NULL '
    
    if host:
        p+="AND host LIKE '{}' ".format(host)

    p+='"'
    
    l="-l core={},walltime={}:00:00".format(core,time)

    n=" "
    if notify:
        import getpass
        if mail:
            n="--notify mail:{} ".format(mail)
        else:
            n="--notify mail:{}@lif.univ-mrs.fr ".format(getpass.getuser())

    t=''
    if besteffort:
        t="-t besteffort -t idempotent"

    a=''
    if anterior is not None:
        a="-a {}".format(anterior)

    if theano:
        python=True
        cuda=True
        flags='THEANO_FLAGS=mode=FAST_RUN,device=gpu0,nvcc.fastmath=True,floatX=float32; {} '.format(flags)
        
    if cuda:
        flags='export PATH=/usr/local/cuda-7.0/bin:$PATH; export LD_LIBRARY_PATH=/usr/local/cuda-7.0/targets/x86_64-linux/lib:$LD_LIBRARY_PATH; {} '.format(flags)

    if python:
        flags='export PYTHONPATH={}; {}'.format(os.environ["PYTHONPATH"], flags)
        
    if interactive:
        command='oarsub -I {} {} {} {} {}'.format(p,l,n,t,a)
    else:
        command='oarsub {} {} {} {} {} "{} {}"'.format(p,l,n,t,a,flags,command)

    if run:
        os.system(command)

    print command
    
    




    
        
